#ifndef DBSERILIZER_H
#define DBSERILIZER_H

#include "sql_builders/insertbuilder.h"
#include "sql_builders/selectbuilder.h"
#include "sql_builders/deletebuilder.h"
#include "sql_builders/updatebuilder.h"
#include "accessors/accessorset.h"
#include "reflection/fields_parser.h"

class DBSerilizer {
public:

    MAST_BE_FIELDS
    void insert(const T &obj) {
        execute(m_insert.make(obj));
    }

    MAST_BE_FULL
    void update(const T &obj) {
        execute(m_update.make(obj));
    }

    MAST_BE_FIELDS
    void clear(const T &obj) {
        execute(m_delete.make(obj));
    }
    MAST_BE_FIELDS
    QVector<T> select() {
        QVector<T> result;
        execute(m_select.make<T>(), &result);
        return result;
    }

private:
    InsertBuilder m_insert;
    UpdateBuilder m_update;
    DeleteBuilder m_delete;
    SelectBuilder m_select;
    void execute(QueryData &&data) const;
    template<typename T>
    void execute(const QString &sql, QVector<T> *vec);
};

template<typename T>
void DBSerilizer::execute(const QString &sql, QVector<T> *vec)
{
    qDebug() << "execute sql make records..." << sql;

    QVariantMap record;

    if(typeName<T>() == "Test_1") {
        record.insert("x", 10);
        record.insert("y", 30);
        record.insert("rad", 11.7f);
    }
    else if(typeName<T>() == "Test_2") {
        record.insert("summ", 11.1);
        record.insert("name", "Text");
        record.insert("id", 42);
        record.insert("current_date", QDate().currentDate());
    }
    else {
       qDebug() << "ПРИКРУТИ БД!!!";
       return;
    }

    T obj; AccessorSetMap access( std::move(record) );
    ns_fields::parseWrite(&obj, access);
    vec->push_back(obj);
}


#endif // DBSERILIZER_H
