#include "accessorget.h"
#include <QtDebug>

const QString d_str = "default access read:";

AccessorGet::~AccessorGet(){}

void AccessorGet::operator()(QString name, int val)
{
    qDebug() << d_str << "type = int    " << " name = " << name << " value = " << val;
}

void AccessorGet::operator()(QString name, short val)
{
    qDebug() << d_str << "type = short  " << " name = " << name << " value = " << val;
}

void AccessorGet::operator()(QString name, float val)
{
    qDebug() << d_str << "type = float  " << " name = " << name << " value = " << val;
}

void AccessorGet::operator()(QString name, double val)
{
    qDebug() << d_str << "type = double " << " name = " << name << " value = " << val;
}

void AccessorGet::operator()(QString name, QString val)
{
    qDebug() << d_str << "type = QString" << " name = " << name << " value = " << val;
}

void AccessorGet::operator()(QString name, QDate val)
{
    qDebug() << d_str << "type = QDate  " << " name = " << name << " value = " << val;
}
