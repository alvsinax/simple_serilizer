#ifndef ACCESSORGET_H
#define ACCESSORGET_H

#include <QString>
#include <QDate>


// дефолтный доступ на чтение структуры: проплевывает результат в QDebug
// при расширении необходимо перегружать под необходимые типы.
class AccessorGet {
public:
    virtual ~AccessorGet();

    virtual void operator()(QString name, int     val);
    virtual void operator()(QString name, short   val);
    virtual void operator()(QString name, float   val);
    virtual void operator()(QString name, double  val);
    virtual void operator()(QString name, QString val);
    virtual void operator()(QString name, QDate   val);
    //...
};


#include <QVariantMap>
class AccessorGetMap : public AccessorGet {
public:
    QVariantMap result() const { return _map; }

    void operator ()(QString name, int val)     override {  _map[name] = val; }
    void operator ()(QString name, short val)   override {  _map[name] = val; }
    void operator ()(QString name, float val)   override {  _map[name] = val; }
    void operator ()(QString name, double val)  override {  _map[name] = val; }
    void operator ()(QString name, QString val) override {  _map[name] = val; }
    void operator ()(QString name, QDate val)   override {  _map[name] = val; }

private:
    QVariantMap _map;
};


#endif // ACCESSORGET_H
