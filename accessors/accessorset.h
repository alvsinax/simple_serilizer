#ifndef ACCESSORSET_H
#define ACCESSORSET_H

#include <QString>
#include <QDate>

// дефолтный доступ на запись структуры: проплевывает результат в QDebug
// при расширении необходимо перегружать под необходимые типы.
class AccessorSet {
public:
    virtual ~AccessorSet();
    virtual void operator()(QString name, int     *val);
    virtual void operator()(QString name, short   *val);
    virtual void operator()(QString name, float   *val);
    virtual void operator()(QString name, double  *val);
    virtual void operator()(QString name, QString *val);
    virtual void operator()(QString name, QDate   *val);
    //...
};


#include <QVariantMap>
class AccessorSetMap : public AccessorSet {
public:
    AccessorSetMap(QVariantMap &&src);

    void operator ()(QString name, int *val) override;
    void operator ()(QString name, short *val) override;
    void operator ()(QString name, float *val) override;
    void operator ()(QString name, double *val) override;
    void operator ()(QString name, QString *val) override;
    void operator ()(QString name, QDate *val) override;

private:
    QVariantMap m_src;
};


#endif // ACCESSORSET_H
