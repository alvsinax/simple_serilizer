#include "accessorset.h"
#include <QtDebug>

const QString d_str = "default access write:";

AccessorSet::~AccessorSet(){}

void AccessorSet::operator()(QString name, int *val)
{
    qDebug() << d_str << "type = int    " << " name = " << name << " value = " << *val;
}

void AccessorSet::operator()(QString name, short *val)
{
    qDebug() << d_str << "type = short  " << " name = " << name << " value = " << *val;
}

void AccessorSet::operator()(QString name, float *val)
{
    qDebug() << d_str << "type = float  " << " name = " << name << " value = " << *val;
}

void AccessorSet::operator()(QString name, double *val)
{
    qDebug() << d_str << "type = double " << " name = " << name << " value = " << *val;
}

void AccessorSet::operator()(QString name, QString *val)
{
    qDebug() << d_str << "type = QString" << " name = " << name << " value = " << *val;
}

void AccessorSet::operator()(QString name, QDate *val)
{
    qDebug() << d_str << "type = QDate  " << " name = " << name << " value = " << *val;
}
//-------------------------------------------------------------------------
AccessorSetMap::AccessorSetMap(QVariantMap &&src) : m_src(src)
{ }

void AccessorSetMap::operator ()(QString name, int *val)
{
    Q_ASSERT(m_src.contains(name));
    *val = m_src[name].toInt();
}

void AccessorSetMap::operator ()(QString name, short *val)
{
    Q_ASSERT(m_src.contains(name));
    *val = static_cast<short>( m_src[name].toInt() );
}

void AccessorSetMap::operator ()(QString name, float *val)
{
    Q_ASSERT(m_src.contains(name));
    *val = m_src[name].toFloat();
}

void AccessorSetMap::operator ()(QString name, double *val)
{
    Q_ASSERT(m_src.contains(name));
    *val = m_src[name].toDouble();
}

void AccessorSetMap::operator ()(QString name, QString *val)
{
    Q_ASSERT(m_src.contains(name));
    *val = m_src[name].toString();
}

void AccessorSetMap::operator ()(QString name, QDate *val)
{
    Q_ASSERT(m_src.contains(name));
    *val = m_src[name].toDate();
}
