#include <QCoreApplication>

#include "sql_builders/insertbuilder.h"
#include "sql_builders/selectbuilder.h"
#include "sql_builders/deletebuilder.h"
#include "sql_builders/updatebuilder.h"

#include "serializers/dbserilizer.h"

#include <QDebug>

//----------------------------------------------------------------
#include <QString>
#include <QDate>
#include "reflection/black_magic.h"

struct Test_1 {
    int   x;
    int   y;
    float rad;

    SERILIZED_FIELDS(Test_1, x, y, rad)
};

struct Test_2 {
    double  summ;
    QString name;
    int     id;
    QDate   current_date;

    SERILIZED_FIELDS(Test_2, summ, name, id, current_date)
    SERILIZED_KEYS(Test_2, id)
};

void example_sql_builders();
void example_db_serilizer();

//----------------------------------------------------------------
// В select заглушка для тестового примера:
// вернет 1 захардкоженное значение для каждого типа.
// пример реализации см. DBSerilizer::execute
// прикручивать реальную БД пока лень, да и незачем на данном этапе.
// для проверок изменения/добавления структур юзай example_sql_builders
//----------------------------------------------------------------
int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    example_db_serilizer();
    //example_sql_builders();

    return a.exec();
}

void example_sql_builders() {
    Test_1 t1 = {10, 32, 11.7f};
    Test_2 t2 = {11.1, "Text", 42, QDate().currentDate()};

    InsertBuilder sql_insert;
    qDebug() << "insert query:";
    qDebug() << "---------------------------------------------------------";
    qDebug() << sql_insert.make(t1);
    qDebug() << sql_insert.make(t2);
    qDebug() << "---------------------------------------------------------";
    SelectBuilder sql_select;
    qDebug() << "select query:";
    qDebug() << "---------------------------------------------------------";
    qDebug() << sql_select.make<Test_1>();
    qDebug() << sql_select.make<Test_2>();
    qDebug() << "---------------------------------------------------------";
    DeleteBuilder sql_delete;
    qDebug() << "delete query:";
    qDebug() << "---------------------------------------------------------";
    qDebug() << sql_delete.make<Test_1>();
    qDebug() << sql_delete.make<Test_2>();
    qDebug() << "---------------------------------------------------------";
    qDebug() << "delete query where:";
    qDebug() << "---------------------------------------------------------";
    qDebug() << sql_delete.make(t1);
    qDebug() << sql_delete.make(t2);
    qDebug() << "---------------------------------------------------------";
    UpdateBuilder sql_update;
    qDebug() << "update query:";
    //qDebug() << sql_update.make(t1); // ошибка компиляции - не заданы ключи!
    qDebug() << sql_update.make(t2);
}

void example_db_serilizer() {

    DBSerilizer db;

    QVector<Test_1> res1 = db.select<Test_1>();
    Test_1 t1 = res1[0];
    QVector<Test_2> res2 = db.select<Test_2>();
    Test_2 t2 = res2[0];

    db.insert(t1);
    db.insert(t2);

    db.clear(t1);
    db.clear(t2);

    //db.update(t1); //ошибка компиляции - нет ключа!
    db.update(t2);
}
