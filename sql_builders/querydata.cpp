#include "querydata.h"


QDebug operator<<(QDebug d, const QueryData &data)
{
    d << "query: \n";
    d << data.sql;
    d << "\n";
    d << "params: \n";
    for(auto s : data.params) {
        d << s.toString();
    }
    return d;
}
