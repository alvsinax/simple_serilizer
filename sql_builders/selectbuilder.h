#ifndef SELECTBUILDER_H
#define SELECTBUILDER_H

#include "reflection/define_names.h"

class SelectBuilder {
public:
    MAST_BE_FIELDS
    QString make() const {
        return makeSql(typeName<T>());
    }
private:
    QString makeSql(const QString &typeName) const;
};

#endif // SELECTBUILDER_H
