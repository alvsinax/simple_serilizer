#include "deletebuilder.h"

QString DeleteBuilder::makeSql(const QString &typeName) const
{
    return "delete from " + typeName + ";";
}

QString DeleteBuilder::makeSql(const QString &typeName, QList<QString> &&names)
{
    QString sql = "delete from " + typeName + " where ";
    QString fieldsArgs;

    const int size = names.count();

    for(int i = 0; i < size - 1; ++i) {
        fieldsArgs.append(QString("%1 = ? and ").arg(names[i]));
    }

    fieldsArgs.append(QString("%1 = ?;").arg(names[size - 1]));
    return sql + fieldsArgs;
}
