#ifndef INSERTBUILDER_H
#define INSERTBUILDER_H

#include "querydata.h"
#include "mapreader.h"

class InsertBuilder {
public:
    MAST_BE_FIELDS
    QueryData make(const T &obj){
        const QString name = typeName<T>();
        QVariantMap map = MapReader::parseFields(obj);
        return { makeSql(name, map.keys()), map.values() };
    }

private:
    QString makeSql(const QString &typeName, QList<QString> &&names);
};


#endif // INSERTBUILDER_H
