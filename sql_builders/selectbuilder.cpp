#include "selectbuilder.h"

QString SelectBuilder::makeSql(const QString &typeName) const
{
    return "select * from " + typeName + ";";
}
