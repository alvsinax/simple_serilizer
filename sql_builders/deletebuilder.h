#ifndef DELETEBUILDER_H
#define DELETEBUILDER_H

#include "querydata.h"
#include "mapreader.h"

class DeleteBuilder {
public:

    MAST_BE_FIELDS
    QString make() const {
        return makeSql(typeName<T>());
    }

    MAST_BE_FIELDS
    QueryData make(const T &obj) {
        const QString name = typeName<T>();

        QVariantMap map = MapReader::parseKeys(obj);
        if(map.isEmpty()) { // ключи не заданы: будем строить по филдам
            map = MapReader::parseFields(obj);
        }
        return { makeSql(name, map.keys()), map.values() };
    }

private:
    QString makeSql(const QString &typeName) const;
    QString makeSql(const QString &typeName, QList<QString> &&names);
};

#endif // DELETEBUILDER_H
