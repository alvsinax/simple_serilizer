#include "updatebuilder.h"

// тут нужно удалить ключи из филдов.
void UpdateBuilder::prepareData(QVariantMap *keys, QVariantMap *fields)
{
    for(auto name : keys->keys()){
        fields->remove(name);
    }
}

QString UpdateBuilder::makeSql(const QString &typeName, QList<QString> &&keys, QList<QString> &&fields)
{
    QString sql = "update from " + typeName + " set ";
    QString args;

    int size = fields.count();

    for(int i = 0; i < size - 1; ++i) {
        args.append(QString("%1 = ?, ").arg(fields[i]));
    }
    args.append(QString("%1 = ?").arg(fields[size - 1]));

    sql.append(args); sql.append(" where ");
    args.clear();

    size = keys.count();
    for(int i = 0; i < size - 1; ++i) {
        sql.append(QString("%1 = ?, ").arg(keys[i]));
    }
    sql.append(QString("%1 = ?").arg(keys[size - 1]));

    return sql;
}
