#ifndef MAPREADER_H
#define MAPREADER_H

#include "reflection/keys_parser.h"
#include "reflection/fields_parser.h"
#include "reflection/define_names.h"
#include "accessors/accessorget.h"

#include <QVariantMap>
#include <QVariantList>

template<typename T, typename = void>
struct ReaderKeys {
    static void work(const T &obj, AccessorGetMap *access) { Q_UNUSED(obj); Q_UNUSED(access); }
};

template<typename T>
struct ReaderKeys<T, decltype(T::NAME_KEYS, void())> {
    static void work(const T &obj, AccessorGetMap *access) {
        ns_keys::parseRead(obj, *(access));
    }
};

template<typename T, typename = void>
struct ReaderFields {
    static void work(const T &obj, AccessorGetMap *access) { Q_UNUSED(obj); Q_UNUSED(access); }
};

template<typename T>
struct ReaderFields<T, decltype(T::NAME_FIELDS, void())> {
    static void work(const T &obj, AccessorGetMap *access) {
        ns_fields::parseRead(obj, *(access));
    }
};

class MapReader {
public:
    template<typename T>
    static QVariantMap parseFields(const T &obj){
        AccessorGetMap access;
        ReaderFields<T>::work(obj, &access);
        return access.result();
    }
    template<typename T>
    static QVariantMap parseKeys(const T &obj) {
        AccessorGetMap access;
        ReaderKeys<T>::work(obj, &access);
        return access.result();
    }
};

#endif // MAPREADER_H
