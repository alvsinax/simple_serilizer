#ifndef QUERYDATA_H
#define QUERYDATA_H

#include <QString>
#include <QVariantList>

#include <QDebug>

struct QueryData {
    QString sql;
    QVariantList params;
};

QDebug operator<<(QDebug d, const QueryData &data);

#endif // QUERYDATA_H
