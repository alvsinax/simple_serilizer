#ifndef UPDATEBUILDER_H
#define UPDATEBUILDER_H

#include "querydata.h"
#include "mapreader.h"

class UpdateBuilder {
public:
    MAST_BE_FULL
    QueryData make(const T &obj) {
        const QString name = typeName<T>();

        QVariantMap keys   = MapReader::parseKeys(obj);
        QVariantMap fields = MapReader::parseFields(obj);
        prepareData(&keys, &fields);
        QVariantList params = fields.values(); params.append(keys.values());
        return { makeSql(name, keys.keys(), fields.keys()), params };
    }

private:
    void prepareData(QVariantMap *keys, QVariantMap *fields);
    QString makeSql(const QString &typeName, QList<QString> &&keys, QList<QString> &&fields);
};

#endif // UPDATEBUILDER_H
