#include "insertbuilder.h"

#include<QtDebug>

QString InsertBuilder::makeSql(const QString &typeName, QList<QString> &&names)
{
   QString sql = "insert into " + typeName + " ( ";
   QString fieldsArgs;
   QString paramsArgs = " values( ";

   const int size = names.count();

   for(int i = 0; i < size - 1; ++i) {
       fieldsArgs.append(QString("%1,").arg(names[i]));
       paramsArgs.append("?,");
   }

   fieldsArgs.append(QString("%1 )").arg(names[size - 1]));
   paramsArgs.append("? );");

   return sql + fieldsArgs +  paramsArgs;
}
