#ifndef DEFINE_NAMES_H
#define DEFINE_NAMES_H

#include "reflection/type_name.h"
#include <QString>

#define NAME_FIELDS properties
#define NAME_KEYS  identifiers

#define MAST_BE_FIELDS template<typename T, typename = std::enable_if_t<HasFilelds<T>::value>>
#define MAST_BE_KEYS   template<typename T, typename = std::enable_if_t<HasKeys<T>::value>>
#define MAST_BE_FULL template<typename T, typename = std::enable_if_t<HasKeys<T>::value>, typename = std::enable_if_t<HasKeys<T>::value>>

template<typename T, typename = void> struct EnableIfHasFilelds{};
template<typename T>
struct EnableIfHasFilelds<T, decltype(T::NAME_FIELDS, void())>{
    typedef void type;
};

template <class T, class = void>
struct HasFilelds {
    constexpr static bool value = false;
};

template <class T>
struct HasFilelds<T, typename EnableIfHasFilelds<T>::type> {
    constexpr static bool value = true;
};

template<typename T, typename = void> struct EnableIfHasKeys{};
template<typename T>
struct EnableIfHasKeys<T, decltype(T::NAME_KEYS, void())>{
    typedef void type;
};

template <class T, class = void>
struct HasKeys {
    constexpr static bool value = false;
};

template <class T>
struct HasKeys<T, typename EnableIfHasKeys<T>::type> {
    constexpr static bool value = true;
};


template<typename T>
QString typeName() {
    QString name;
    auto res  = type_name<T>();
    auto data = res.data();
    for(std::size_t i = 0; i < res.size(); i++) {
        name.append(data[i]);
    }
    return name;
}


#endif // DEFINE_NAMES_H
