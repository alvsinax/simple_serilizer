#ifndef KEYS_PARSER_H
#define KEYS_PARSER_H

#include <tuple>
#include <utility>

#include "define_names.h"

namespace ns_keys {

template<std::size_t iteration, typename T, typename A>
void accessGet(T&& object, A&& access){
   constexpr auto property = std::get<iteration>(std::decay_t<T>::NAME_KEYS);
   std::forward<A>(access)(property.name, std::forward<T>(object).*(property.member));
}

template<std::size_t iteration, typename T, typename A>
std::enable_if_t<(iteration > 0)>
accessGetIteration(T&& object, A&& access) {
    accessGet<iteration>(std::forward<T>(object), std::forward<A>(access));
    accessGetIteration<iteration - 1>(std::forward<T>(object), std::forward<A>(access));
}

template<std::size_t iteration, typename T, typename A>
std::enable_if_t<(iteration == 0)>
accessGetIteration(T&& object, A&& access) {
    accessGet<iteration>(std::forward<T>(object), std::forward<A>(access));
}

template<typename T, typename A>
void parseRead(T&& object, A&& access) {
    accessGetIteration<std::tuple_size<decltype(std::decay_t<T>::NAME_KEYS)>::value - 1>(
        std::forward<T>(object), std::forward<A>(access)
    );
}

}

#endif // IDENTITY_PARSER_H
