#ifndef BLACK_MAGIC_H
#define BLACK_MAGIC_H

#include "reflection/property.h"
#include "define_names.h"

#define CONCAT(a, b) a ## b
#define CONCAT2(a, b) CONCAT(a, b)

#define PP_NARG(...) \
         PP_NARG_(__VA_ARGS__,PP_RSEQ_N())
#define PP_NARG_(...) \
         PP_ARG_N(__VA_ARGS__)
#define PP_ARG_N( \
          _1, _2, _3, _4, _5, _6, _7, _8, _9,_10, \
         _11,_12,_13,_14,_15,_16,_17,_18,_19,_20, \
         _21,_22,_23,_24,_25,_26,_27,_28,_29,_30, \
         _31,_32,_33,_34,_35,_36,_37,_38,_39,_40, \
         _41,_42,_43,_44,_45,_46,_47,_48,_49,_50, \
         _51,_52,_53,_54,_55,_56,_57,_58,_59,_60, \
         _61,_62,_63,N,...) N
#define PP_RSEQ_N() \
         63,62,61,60,                   \
         59,58,57,56,55,54,53,52,51,50, \
         49,48,47,46,45,44,43,42,41,40, \
         39,38,37,36,35,34,33,32,31,30, \
         29,28,27,26,25,24,23,22,21,20, \
         19,18,17,16,15,14,13,12,11,10, \
         9,8,7,6,5,4,3,2,1,0

#define MAKE_FIELD_ARG_ARGUMENTS(Type, ...) CONCAT2(MAKE_FIELD_ARG_, PP_NARG(__VA_ARGS__))(Type, ##__VA_ARGS__)

#define MAKE_FIELD(Type, Field) \
    makeProperty(&Type::Field, #Field)

#define SERILIZED_FIELDS(Type, ...) \
    constexpr static auto NAME_FIELDS = std::make_tuple ( \
        MAKE_FIELD_ARG_ARGUMENTS(Type, __VA_ARGS__)      \
    );

#define SERILIZED_KEYS(Type, ...) \
    constexpr static auto NAME_KEYS = std::make_tuple ( \
        MAKE_FIELD_ARG_ARGUMENTS(Type, __VA_ARGS__)      \
    );

#define MAKE_FIELD_ARG_1(Type, Field) \
    MAKE_FIELD(Type, Field)

#define MAKE_FIELD_ARG_2(Type, Field, ...) \
    MAKE_FIELD(Type, Field),    \
    MAKE_FIELD_ARG_1(Type, __VA_ARGS__)

#define MAKE_FIELD_ARG_3(Type, Field, ...) \
    MAKE_FIELD(Type, Field),    \
    MAKE_FIELD_ARG_2(Type, __VA_ARGS__)

#define MAKE_FIELD_ARG_4(Type, Field, ...) \
    MAKE_FIELD(Type, Field),    \
    MAKE_FIELD_ARG_3(Type, __VA_ARGS__)

#define MAKE_FIELD_ARG_5(Type, Field, ...) \
    MAKE_FIELD(Type, Field),    \
    MAKE_FIELD_ARG_4(Type, __VA_ARGS__)

#define MAKE_FIELD_ARG_6(Type, Field, ...) \
    MAKE_FIELD(Type, Field),    \
    MAKE_FIELD_ARG_5(Type, __VA_ARGS__)

#define MAKE_FIELD_ARG_7(Type, Field, ...) \
    MAKE_FIELD(Type, Field),    \
    MAKE_FIELD_ARG_6(Type, __VA_ARGS__)

#define MAKE_FIELD_ARG_8(Type, Field, ...) \
    MAKE_FIELD(Type, Field),    \
    MAKE_FIELD_ARG_7(Type, __VA_ARGS__)

#define MAKE_FIELD_ARG_9(Type, Field, ...) \
    MAKE_FIELD(Type, Field),    \
    MAKE_FIELD_ARG_8(Type, __VA_ARGS__)

#define MAKE_FIELD_ARG_10(Type, Field, ...) \
    MAKE_FIELD(Type, Field),    \
    MAKE_FIELD_ARG_9(Type, __VA_ARGS__)

#define MAKE_FIELD_ARG_11(Type, Field, ...) \
    MAKE_FIELD(Type, Field),    \
    MAKE_FIELD_ARG_10(Type, __VA_ARGS__)

#define MAKE_FIELD_ARG_12(Type, Field, ...) \
    MAKE_FIELD(Type, Field),    \
    MAKE_FIELD_ARG_11(Type, __VA_ARGS__)

#define MAKE_FIELD_ARG_13(Type, Field, ...) \
    MAKE_FIELD(Type, Field),    \
    MAKE_FIELD_ARG_12(Type, __VA_ARGS__)

#define MAKE_FIELD_ARG_14(Type, Field, ...) \
    MAKE_FIELD(Type, Field),    \
    MAKE_FIELD_ARG_13(Type, __VA_ARGS__)

#define MAKE_FIELD_ARG_15(Type, Field, ...) \
    MAKE_FIELD(Type, Field),    \
    MAKE_FIELD_ARG_14(Type, __VA_ARGS__)

#define MAKE_FIELD_ARG_16(Type, Field, ...) \
    MAKE_FIELD(Type, Field),    \
    MAKE_FIELD_ARG_15(Type, __VA_ARGS__)

#define MAKE_FIELD_ARG_17(Type, Field, ...) \
    MAKE_FIELD(Type, Field),    \
    MAKE_FIELD_ARG_16(Type, __VA_ARGS__)

#define MAKE_FIELD_ARG_18(Type, Field, ...) \
    MAKE_FIELD(Type, Field),    \
    MAKE_FIELD_ARG_17(Type, __VA_ARGS__)

#define MAKE_FIELD_ARG_19(Type, Field, ...) \
    MAKE_FIELD(Type, Field),    \
    MAKE_FIELD_ARG_18(Type, __VA_ARGS__)

#define MAKE_FIELD_ARG_20(Type, Field, ...) \
    MAKE_FIELD(Type, Field),    \
    MAKE_FIELD_ARG_19(Type, __VA_ARGS__)

#define MAKE_FIELD_ARG_21(Type, Field, ...) \
    MAKE_FIELD(Type, Field),    \
    MAKE_FIELD_ARG_20(Type, __VA_ARGS__)

#endif // BLACK_MAGIC_H
