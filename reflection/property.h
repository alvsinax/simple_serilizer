#ifndef PROPERTY_H
#define PROPERTY_H

#include <tuple>

template<typename Class, typename T>
struct Property {
    constexpr Property(T Class::*aMember, const char* aName) : member{aMember}, name{aName} {}

    using Type = T;

    T Class::*member;
    const char* name;
};

template<typename Class, typename T>
constexpr auto makeProperty(T Class::*member, const char* name) {
    return Property<Class, T>{member, name};
}

#endif // PROPERTY_H
