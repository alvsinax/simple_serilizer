#ifndef FIELDS_PARSER_H
#define FIELDS_PARSER_H

#include <tuple>
#include <utility>

#include "define_names.h"

namespace ns_fields {

//------------------------------------------------------------------------------------------
template<std::size_t iteration, typename T, typename A>
void accessGet(T&& object, A&& access){
   constexpr auto property = std::get<iteration>(std::decay_t<T>::NAME_FIELDS);
   std::forward<A>(access)(property.name, std::forward<T>(object).*(property.member));   
}

template<std::size_t iteration, typename T, typename A>
std::enable_if_t<(iteration == 0)>
accessGetIteration(T&& object, A&& access) {
    accessGet<iteration>(std::forward<T>(object), std::forward<A>(access));
}

template<std::size_t iteration, typename T, typename A>
std::enable_if_t<(iteration > 0)>
accessGetIteration(T&& object, A&& access) {
    accessGet<iteration>(std::forward<T>(object), std::forward<A>(access));
    accessGetIteration<iteration - 1>(std::forward<T>(object), std::forward<A>(access));
}

template<typename T, typename A>
void parseRead(T&& object, A&& access) {
    accessGetIteration<std::tuple_size<decltype(std::decay_t<T>::NAME_FIELDS)>::value - 1>(
        std::forward<T>(object), std::forward<A>(access)
    );
}
//------------------------------------------------------------------------------------------

template<std::size_t iteration, typename T, typename A>
void accessSet(T* object, A&& access){
   constexpr auto property = std::get<iteration>(std::decay_t<T>::NAME_FIELDS);
   auto val = (object)->*(property.member);
   std::forward<A>(access)(property.name, &val);
   (object)->*(property.member) = val;
}

template<std::size_t iteration, typename T, typename A>
std::enable_if_t<(iteration == 0)>
accessSetIteration(T* object, A&& access) {
    accessSet<iteration>(object, std::forward<A>(access));
}

template<std::size_t iteration, typename T, typename A>
std::enable_if_t<(iteration > 0)>
accessSetIteration(T* object, A&& access) {
    accessSet<iteration>(object, std::forward<A>(access));
    accessSetIteration<iteration - 1>(object, std::forward<A>(access));
}

template<typename T, typename A>
void parseWrite(T *object, A&& access) {
    accessSetIteration<std::tuple_size<decltype(std::decay_t<T>::NAME_FIELDS)>::value - 1>(
        object, std::forward<A>(access)
    );
}

}

#endif // PROPERTY_PARSER_H
