QT -= gui

CONFIG += c++14 console
CONFIG -= app_bundle

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        main.cpp \
    accessors/accessorget.cpp \
    sql_builders/insertbuilder.cpp \
    sql_builders/querydata.cpp \
    sql_builders/selectbuilder.cpp \
    sql_builders/deletebuilder.cpp \
    sql_builders/updatebuilder.cpp \
    serializers/dbserilizer.cpp \
    accessors/accessorset.cpp

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    reflection/property.h \
    reflection/type_name.h \
    reflection/black_magic.h \
    reflection/define_names.h \
    accessors/accessorget.h \
    sql_builders/querydata.h \
    sql_builders/insertbuilder.h \
    sql_builders/selectbuilder.h \
    sql_builders/deletebuilder.h \
    sql_builders/mapreader.h \
    reflection/keys_parser.h \
    reflection/fields_parser.h \
    sql_builders/updatebuilder.h \
    serializers/dbserilizer.h \
    accessors/accessorset.h
